import express = require("express");
import dataset from "./hotels";

/**
 * Small REST API for serving already-sorted and fíltered versions of the information  
 */
const app = express();

console.log("Building dataset");

// Build a name-indexed map of hotels to quickly filter by name:
const hotelsByName = {} as any;

// How many different names there are:
let differentNames = 0;

// Array containing the data
const hotels = dataset.Establishments
// Mutate the data to give it an index from 0 to size-1:
hotels.forEach((hotel, index) => {
  hotel.index = index;
  if (!(hotel.Name in hotelsByName)) {
    differentNames += 1;
    hotelsByName[hotel.Name] = [];
  }
  hotelsByName[hotel.Name].push(index);
});

const amount = hotels.length;
const lastHotel = hotels.length - 1;

/**
 * Sort data by: 
 * Distance
 * Stars
 * MinCost
 * TrpRating
 * UserRating
 */

// By distance:
const hotelsByDistance = hotels.sort((a, b) => a.Distance - b.Distance)
  // save memory by keeping only the index:
  .map((hotel) => hotel.index);

// By stars:
const hotelsByStars = hotels.sort((a, b) => a.Stars - b.Stars)
  // save memory by keeping only the index:
  .map((hotel) => hotel.index);

// By cost:
const hotelsByCost = hotels.sort((a, b) => a.MinCost - b.MinCost)
  // save memory by keeping only the index:
  .map((hotel) => hotel.index);

// By TrpRating:
// I don't see any TrpRating field. I'm unsure about what should I give here,
// as there only UserRating in the dataset.

// By UserRating: 
const hotelsByUserRating = hotels.sort((a, b) => a.UserRating - b.UserRating)
  // save memory by keeping only the index:
  .map((hotel) => hotel.index);

// Recover original sorting:
hotels.sort((a, b) => a.index - b.index);

// The maximum distance:
const maxDistance = hotels[hotelsByDistance[lastHotel]].Distance;

// The minimum distance:
const minDistance = hotels[hotelsByDistance[0]].Distance;

// The maximum stars:
const maxStars = hotels[hotelsByStars[lastHotel]].Stars;

// The minimum stars:
const minStars = hotels[hotelsByStars[0]].Stars;

// The maximum cost:
const maxCost = hotels[hotelsByCost[lastHotel]].MinCost;

// The minimum cost:
const minCost = hotels[hotelsByCost[0]].MinCost;

// The maximum UserRating:
const maxUserRating = hotels[hotelsByUserRating[lastHotel]].UserRating;

// The minimum UserRating:
const minUserRating = hotels[hotelsByUserRating[0]].UserRating;

console.log("Dataset built");

// Return the probable size of dataset by assuming the data is distributed as a uniform distribution:
function uniformDatasetSize(minValue: number, maxValue: number, range: [number, number]) {
  const valueSize = maxValue - minValue;
  const rangeSize = Math.min(range[1], maxValue) - Math.max(range[0], minValue);
  return (rangeSize / valueSize) * amount;
}

// Filter by name:
function getHotelsByName(name: string) {
  return hotelsByName[name] || [];
}

// Filter an array by a field from a range of values
function getHotelsByFilteringArray(field: string, arrayOfIndex: number[], range: [number | string, number]) {
  // Avoid filtering by name:
  if (field === "Name") {
    return getHotelsByName(range[0] as string);
  }
  return arrayOfIndex.filter((el) => {
    return hotels[el][field] >= range[0] && hotels[el][field] <= range[1];
  });
}

// Filter an array by a field from a range of values,
// expecting the array to be sorted by its field
// so we can apply binary search to reduce dramatically the time of filtering
// the data.
function getHotelsByFilteringWithBinarySearch(field: string, sortedArrayOfIndex: number[], range: [number | string, number]) {
  // Avoid filtering by name:
  if (field === "Name") {
    return getHotelsByName(range[0] as string);
  }

  // Find upper bound of lower range:
  let lb = 0, hb = amount;

  while (lb < hb - 1) {
    const mb = ((hb + lb) / 2 + 0.5) | 0;
    if (hotels[sortedArrayOfIndex[mb]][field] >= range[0]) {
      hb = mb;
    } else {
      lb = mb;
    }
  }
  let firstElement = lb;
  if (hotels[sortedArrayOfIndex[lb]][field] < range[0])
    firstElement = hb;

  // Find lower bound of upper range:
  // Start by the first element we found in the last
  // iteration, to save time
  lb = firstElement, hb = amount;

  while (lb < hb - 1) {
    const mb = ((hb + lb) / 2 + 0.5) | 0;
    if (hotels[sortedArrayOfIndex[mb]][field] <= range[1]) {
      lb = mb;
    } else {
      hb = mb;
    }
  }

  const lastElement = hb;
  return sortedArrayOfIndex.slice(firstElement, lastElement);
}

// Probable sizes for data filters:
const filters = {
  // Filtering by name gives a constant aproximation of the uniform distribution.
  Name: (name) => amount / differentNames,
  Stars: (range) => uniformDatasetSize(minStars, maxStars, range),
  // TrpRating: () => ...,
  UserRating: (range) => uniformDatasetSize(minUserRating, maxUserRating, range),
  MinCost: (range) => uniformDatasetSize(minCost, maxCost, range),
}

// Allow COSR 
// all origins: bad practice, but enough for this test:
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');

  next();
})

// Express middleware to measure request time:
app.use((req, res, next) => {
  req.initTime = new Date();
  next();
});

// Express middleware to get filters
app.use((req, res, next) => {
  req.filters = [];
  for (const k in req.query) {
    const match = /^filter([a-zA-Z]+)/.exec(k);
    if (match) {
      const name = match[1];
      const range = JSON.parse(req.query[k]);
      req.filters.push({
        filterName: name,
        range: range,
        proableSize: filters[name](range),
      });
    }
  }

  // Sort filters by probable size:
  req.filters.sort((a, b) => {
    return a.proableSize - b.proableSize;
  });
  next();
});

const sortedArrayIndexes = {
  Name: hotelsByName,
  Stars: hotelsByStars,
  // TrpRating: hotelsBy...,
  UserRating: hotelsByUserRating,
  MinCost: hotelsByCost,
}

// Return the query by applying the filter and sort 
app.get("/", (req, res, next) => {
  if (req.filters.length) {
    // First of all, we filter by the smallest probable subset,
    // so we have a smaller set to apply the rest of the filters.
    // The smaller the set of hotels, the faster the filter is done. 
    const firstFilter = req.filters[0];
    let array = getHotelsByFilteringWithBinarySearch(firstFilter.filterName,
      sortedArrayIndexes[firstFilter.filterName],
      firstFilter.range);

    // After applying the first filter, we loop through the rest of the filters:
    for (let i = 1; i < req.filters.length; i++) {
      array = getHotelsByFilteringArray(req.filters[i].filterName, array, req.filters[i].range);
    }
    res.send(array.map((el) => hotels[el]));
  } else {
    // If no filter, we send everything
    res.send(hotels);
  }
  next();
})

// Finally measure time:
app.use((req, res, next) => {
  console.log(req.method + " " + req.path + " " + (+new Date() - +req.initTime) + "ms");
  next();
})

app.listen(3000, () => {
  console.log("Listening to 3000");
});
