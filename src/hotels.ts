const hotels = require("../hotels.json")

interface IHotels {
  AvailabilitySearchId: string;
  Establishments: IHotel[];
}

interface IHotel {
  index: number;
  Distance: number;
  EstablishmentId: number;
  EstablishmentType: string;
  Location: string;
  MinCost: number;
  Name: string;
  Stars: number;
  UserRating: number;
  UserRatingTitle: string;
  UserRatingCount: number;
  ImageUrl: string;
  ThumbnailUrl: string;
}

export default hotels as IHotels;