declare global {
  export namespace Express {
    interface Request {
      filters: Array<{
        filterName: string;
        range: [number|string, number];
        proableSize: number; 
      }>;
      initTime: Date;
    }
  }
}

// Dummy export.
export const x = "";